string deviceModel = string.Empty;
string deviceManufactor = string.Empty;
string[] properties = { "System.Devices.ModelName", "System.Devices.Manufacturer" };
string aqs = "System.Devices.LocalMachine:=System.StructuredQueryType.Boolean#True";//过滤条件：只选取localMachine为true的

var containers = await PnpObject.FindAllAsync(PnpObjectType.DeviceContainer, properties, aqs);//查找出当前机器
{
        var container = containers[0];
        deviceModel = container.Properties[properties[0]].ToString();
        deviceManufactor = container.Properties[properties[1]].ToString();
}